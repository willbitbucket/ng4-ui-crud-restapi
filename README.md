### Getting Started

This is Angular 4 UI for asset management console.
Currently it has login/logout and assets page, with ability to CRUD an asset.

#### screenshot

```
cd ./screenshot
```

#### Run

```
install ng cli first
> npm install
> ng serve --open
```
#### Ports
WebApp port: 4200

#### App
WebApp running

#### Note
The Product model in src code is equivalent to the Asset

#### Todo
1. Unit tests need to be fixed
2. ng select binding is not properly used
3. large refactoring is required to be production ready
