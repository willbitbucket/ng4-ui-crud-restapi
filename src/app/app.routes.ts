import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { productsRoutes }    from './products/products.routes';
import { LoginComponent } from './login/index';
import { ProductsListComponent } from './products/products-list/products-list.component';

export const routes: Routes = [
  {
    path: '',
    component: ProductsListComponent,
    pathMatch: 'full'
  },
  { path: 'login', component: LoginComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
