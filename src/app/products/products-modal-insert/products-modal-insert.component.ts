import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Product } from '../models/product';
import { ProductsService } from '../services/products.service';
import {
  ReactiveFormsModule,
  FormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';
import { ShoppingCentre } from '../models/shoppingCentre'

@Component({
  selector: 'app-products-modal-insert',
  templateUrl: './products-modal-insert.component.html',
  styleUrls: ['./products-modal-insert.component.css']
})
export class ProductsModalInsertComponent implements OnInit {
  modalForm: FormGroup;
  title: string;
  product: Product = new Product();
  listSC: ShoppingCentre[];
  loading: any;
  state: any;
  constructor(private productService: ProductsService, public bsModalRef: BsModalRef) {
    this.productService.listSC$.subscribe(
    data => {
      this.listSC = data.map( sc => {
        return sc;
      });
    },
    err => {
      console.log("Error getting sc (check node server) ", err);
  });
  this.productService.loadingListSC$.subscribe(
    response => this.loading = response
  );
  }

  ngOnInit() {
    this.state = {message :'', selectedShoppingCentre: null};
    this.productService.getAllShoppingCentre();

    this.modalForm = new FormGroup({
      name: new FormControl(this.product.name, [

      ]),
      location: new FormControl(this.product.location, [

      ]),
      dimensions: new FormControl(this.product.dimensions, [

      ]),
      status: new FormControl(this.product.status, [

      ]),
      language: new FormControl(),
    });
  }
  updateShoppingCentre(e):void{
    this.state.selectedShoppingCentre = e;
  }
  updateStatus(e):void{
    this.product.status = e;
  }
  onClickSave():void {
    
    this.product.shoppingCentre = this.state.selectedShoppingCentre;
    this.productService.add(this.product).subscribe(
			response => {
        this.bsModalRef.hide();
        this.bsModalRef = null;
			},
			err => {
				console.log("Error insert (check node server) ", err);
		});
  }

}
