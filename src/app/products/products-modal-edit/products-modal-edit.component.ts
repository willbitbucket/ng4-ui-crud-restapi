import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Product } from '../models/product';
import { ShoppingCentre } from '../models/shoppingCentre'

import { ProductsService } from '../services/products.service';
import {
  ReactiveFormsModule,
  FormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
@Component({
  selector: 'app-products-modal-edit',
  templateUrl: './products-modal-edit.component.html',
  styleUrls: ['./products-modal-edit.component.css']
})
export class ProductsModalEditComponent implements OnInit {
  modalForm: FormGroup;
  title: string;
  product: Product = new Product();
  state: any;
  listSC: ShoppingCentre[];
  loading: any;
  public statusOptions: Array<Select2OptionData>;
  constructor(private productService: ProductsService, public bsModalRef: BsModalRef) {
    this.productService.listSC$.subscribe(
    data => {
      this.listSC = data.map( sc => {
        return sc;
      });
    },
    err => {
      console.log("Error getting sc (check node server) ", err);
  });
  this.productService.loadingListSC$.subscribe(
    response => this.loading = response
  );
  }


  ngOnInit() {
    this.state = {message :'', selectedShoppingCentre: null};
    this.productService.getAllShoppingCentre();
    this.modalForm = new FormGroup({
      name: new FormControl(this.product.name, [

      ]),
      location: new FormControl(this.product.location, [

      ]),
      dimensions: new FormControl(this.product.dimensions, [

      ]),
      status: new FormControl(this.product.status, [

      ]),
      language: new FormControl(),
    });
  }

  updateShoppingCentre(e):void{
    this.state.selectedShoppingCentre = e;
  }
  updateStatus(e):void{
    this.product.status = e;
  }
  onClickSave():void {

    let update = {...{}, ...this.product}
    console.log('(this.state.selectedShoppingCentre',this.state.selectedShoppingCentre);
    if (this.state.selectedShoppingCentre) update.shoppingCentre = this.state.selectedShoppingCentre;
    else update.shoppingCentre = this.product.shoppingCentre._id;

    console.log('update',update)
    this.productService.update(update).subscribe(
			response => {
        this.bsModalRef.hide();
        this.bsModalRef = null;
			},
			err => {
        alert(err.message);
      //  this.product.shoppingCentre = shoppingCentre;
      },
      () => console.log("Ended")
  );
  }

}
