import { TestBed, inject } from '@angular/core/testing';

import { ProductsService } from './products.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
describe('ProductsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductsService],
      schemas: [
        NO_ERRORS_SCHEMA //https://stackoverflow.com/questions/39428132/custom-elements-schema-added-to-ngmodule-schemas-still-showing-error
      ],
    });
  });

  it('should be created', inject([ProductsService], (service: ProductsService) => {
    expect(service).toBeTruthy();
  }));
});
