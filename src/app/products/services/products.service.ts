import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from "@angular/http";
import { Observable } from 'rxjs/Rx';
import { Subject }    from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Product } from '../models/product'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../../environments/environment';
import { ShoppingCentre } from '../models/shoppingCentre'

@Injectable()
export class ProductsService {
	private url = `${environment.apiUrl}/api/assets`;

	private list = new Subject<Product[]>();
  	list$ = this.list.asObservable();

	private loadingList = new BehaviorSubject<boolean>(false);
	loadingList$ = this.loadingList.asObservable();

	private inserted = new BehaviorSubject<boolean>(false);
	inserted$ = this.inserted.asObservable();

	private updated = new BehaviorSubject<boolean>(false);
	updated$ = this.updated.asObservable();

	private deleted = new BehaviorSubject<boolean>(false);
	deleted$ = this.deleted.asObservable();

	private listSC = new Subject<ShoppingCentre[]>();
  	listSC$ = this.listSC.asObservable();
	private loadingListSC = new BehaviorSubject<boolean>(false);
		loadingListSC$ = this.loadingListSC.asObservable();


  private headers = new Headers({
		'Content-Type' : 'application/json'
	});

	constructor (private http: Http) {}

	securedHeaders():Headers {
		const user =  JSON.parse(localStorage.getItem('currentUser'));
		if (user && user.token)
		return new Headers({
			'Content-Type' : 'application/json',
			'Authorization' : 'Bearer ' + user.token
			});
		else return this.headers;
	}

	getAll(condition:any):void {
		let query = '?';
		if (condition){
			if (condition.name) query +=`name=${condition.name}&`;
			if (condition.status) query +=`status=${condition.status}&`;
			if (condition.shoppingCentre) query +=`shoppingCentre=${condition.shoppingCentre}&`;
		}
		this.loadingList.next(true);
		this.http.get(this.url+query, {headers:this.securedHeaders()})
		.map((res:Response) => this.list.next(res.json()))
		.finally(() => this.loadingList.next(false))
		.catch((error:any) => {
			this.list.error(new Error(error || 'Server error'));
			return Observable.throw(error.json().error || 'Server error')
		}).subscribe();
	}

	getAllShoppingCentre():void {
		const nullSC = new ShoppingCentre();
		nullSC._id=null;
		this.loadingListSC.next(true);
		this.http.get(`${environment.apiUrl}/api/shoppingCentres`, {headers:this.securedHeaders()})
		.map((res:Response) => this.listSC.next([nullSC].concat(res.json())))
		.finally(() => this.loadingListSC.next(false))
		.catch((error:any) => {
			this.listSC.error(new Error(error || 'Server error'));
			return Observable.throw(error.json().error || 'Server error')
		}).subscribe();
	}

	add(product: Product): Observable<void>  {
		return this.http.post(this.url, product, {headers:this.securedHeaders()})
		.map((res:Response) => this.inserted.next(true));
    }

	update(product: Product): Observable<void> {
		console.log('b4',product);
		return this.http.put(`${this.url}/${product._id}`, product,  {headers:this.securedHeaders()})
		/*.map(res => res.json())
	  .subscribe(
	    data => {console.log(data); this.updated.next(true)},
	    err => console.log('err',err),
	    () => console.log('yay')
	  );*/

		.map((res:Response) => {
			console.log('this.updated.next(true)');
			this.updated.next(true)
		})
		.catch( res => Observable.throw(res.json()));

	}

	delete(product: Product): Observable<void> {
		return this.http.delete(`${this.url}/${product._id}`, {headers:this.securedHeaders()})
		.map((res:Response) => this.deleted.next(true));
    }
}
