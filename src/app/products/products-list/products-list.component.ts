import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProductsService } from '../services/products.service';
import { Product } from '../models/product'
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { ProductsModalInsertComponent } from '../products-modal-insert/products-modal-insert.component';
import { ShoppingCentre } from '../models/shoppingCentre'
@Component({
	selector: 'app-products-list',
	templateUrl: './products-list.component.html',
	styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
	public alerts: any = [];
	bsModalRef: BsModalRef;
	productList: Product[];
	shoppingCentreList: ShoppingCentre[];
	loading: boolean;
	inserted: boolean;
	search: any;

  	constructor(private productService: ProductsService, private modalService: BsModalService) {
		 this.productService.listSC$.subscribe(
		    data => {
		      this.shoppingCentreList = data.map( sc => {
		        return sc;
		      });
		    },
		    err => {
		      console.log("Error getting sc (check node server) ", err);
		  });
			this.productService.list$.subscribe(
			data => {
				this.productList = data.map( product => {
					return product;
				});
			},
			err => {
				console.log("Error getting list (check node server) ", err);
		});
		this.productService.loadingList$.subscribe(
			response => this.loading = response
		);
		this.productService.inserted$.subscribe(
			response => {
				if(response===true){
					this.alerts.push({
						type: 'md-local',
						msg: `Product added correctly`,
						timeout: 6000
					});
					this.loadData();
				}
			}
		);
		this.productService.updated$.subscribe(
			response => {
				if(response===true){
					this.alerts.push({
						type: 'warning',
						msg: `Product updated correctly`,
						timeout: 6000
					});
					this.loadData();
				}
			}
		);
		this.productService.deleted$.subscribe(
			response => {
				if(response===true){
					this.alerts.push({
						type: 'danger',
						msg: `Product deleted correctly`,
						timeout: 6000
					});
					this.loadData();
				}
			}
		);
	}

	ngOnInit() {
		this.search = {};
		this.loadData();
	}

	loadData():void {
		this.productService.getAll('');
		this.productService.getAllShoppingCentre();
	}

	updateShoppingCentre(e):void{
		this.search.shoppingCentre = e;
	}
	updateStatus(e):void{
		this.search.status = e;
	}
	onClickSearch():void{
		this.productService.getAll(this.search);
	}
	openModalInsert(event: Event):void{
		event.preventDefault();
		this.bsModalRef = this.modalService.show(ProductsModalInsertComponent);
		this.bsModalRef.content.title = `Insert Asset`;
	}


}
