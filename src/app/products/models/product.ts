export class Product {
    public _id: string;
    public name: string;
    public location: string;
    public dimensions: string;
    public shoppingCentre: any;
    public __v: number;
    public status: string;

}
